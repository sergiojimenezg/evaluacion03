<%-- 
    Document   : index
    Created on : 07-may-2020, 22:42:21
    Author     : sjimenez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h2>Evaluacion 03: Sergio Jimenez</h2>
            <h3>Documentación:</h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Metodo</th>
                        <th>URL</th>
                        <th>Requisitos</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Listar Autos (Metodo GET)</th>
                        <th><a href="/api/autos">/api/autos</a></th>
                        <th>N/A</th>
                    </tr>
                    <tr>
                        <th>Buscar Auto (Metodo GET)</th>
                        <th><a href="/api/autos/{idbuscar}">/api/vehiculos/{idbuscar}</a></th>
                        <th>Debe reemplazar {idbuscar} por el id del registro del vehiculo</th>
                    </tr>
                    <tr>
                        <th>Agregar Vehiculo (Metodo POST)</th>
                        <th><a href="/api/autos">/api/autos</a></th>
                        <th>Se deben ingresar los siguientes datos: <br>
                            {
                                "año":, 
                                "color":,
                                "id":,
                                "marca":,
                                "modelo":,
                            }
                        </th>
                    </tr>
                    <tr>
                        <th>Actualizar Registro (Metodo PUT)</th>
                        <th><a href="/api/autos">/api/autos</a></th>
                        <th>Se deben ingresar los siguientes datos para modificar: <br>
                            {
                                "año":, 
                                "color":,
                                "id":,
                                "marca":,
                                "modelo":,
                            }
                        </th>
                    </tr>
                    <tr>
                        <th>Eliminar Registro (Metodo DELETE)</th>
                        <th><a href="/api/autos/{iddelete}">/api/autos/{iddelete}</a></th>
                        <th>Debe reemplazar {idbuscar} por el id del auto a eliminar</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
