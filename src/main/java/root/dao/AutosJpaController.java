/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import root.presistence.entities.Autos;

/**
 *
 * @author sjimenez
 */
public class AutosJpaController implements Serializable {

    public AutosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public AutosJpaController() {
    }
    
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("Autos_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Autos autos) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(autos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAutos(autos.getId()) != null) {
                throw new PreexistingEntityException("Autos " + autos + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Autos autos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            autos = em.merge(autos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = autos.getId();
                if (findAutos(id) == null) {
                    throw new NonexistentEntityException("The autos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Autos autos;
            try {
                autos = em.getReference(Autos.class, id);
                autos.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The autos with id " + id + " no longer exists.", enfe);
            }
            em.remove(autos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Autos> findAutosEntities() {
        return findAutosEntities(true, -1, -1);
    }

    public List<Autos> findAutosEntities(int maxResults, int firstResult) {
        return findAutosEntities(false, maxResults, firstResult);
    }

    private List<Autos> findAutosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Autos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Autos findAutos(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Autos.class, id);
        } finally {
            em.close();
        }
    }

    public int getAutosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Autos> rt = cq.from(Autos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
