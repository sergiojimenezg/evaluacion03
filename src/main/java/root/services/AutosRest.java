
package root.services;


import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.AutosJpaController;
import root.dao.exceptions.NonexistentEntityException;
import root.presistence.entities.Autos;





@Path("autos")
public class AutosRest {
    
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Autos_PU");
    EntityManager em;
    
    AutosJpaController dao = new AutosJpaController();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response listarTodo(){
        
        List<Autos> lista = dao.findAutosEntities();
        return Response.ok(200).entity(lista).build();
    }
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response buscarId(@PathParam("idbuscar") String idbuscar){
        
        Autos auto = dao.findAutos(idbuscar);
        return Response.ok(200).entity(auto).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String agregar (Autos nuevoauto) throws Exception{
        
        String ver = nuevoauto.getId();
        Autos en = dao.findAutos(ver);
        
        String salida = "";
        if(en!=null){
            salida = "** AUTO NO REGISTRADO **\n ver: " + nuevoauto.getId() + " Ya Existe";
        }
        else{
            dao.create(nuevoauto);
            salida = "Auto ingresado";
        }
        
        return salida;
        
    }
    
    @PUT
    public Response actualizar (Autos autosupdate) throws Exception{
        
        dao.edit(autosupdate);
        return Response.ok(autosupdate).build();
        
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    
    public Response eliminar (@PathParam("iddelete") String iddelete) throws NonexistentEntityException {
        
        dao.destroy(iddelete);
        return Response.ok("eliminado").build();
    }
    
    
}
